First react app I've made.
deployed version: https://react-todo-app-rust-five.vercel.app/
features:
- Create todo
- Check completed todos
- Clear completed todos
- Todos stored to local storage
